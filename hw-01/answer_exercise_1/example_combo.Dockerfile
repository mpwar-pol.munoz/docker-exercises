FROM alpine:3.12.3
ENTRYPOINT ["ping"]
CMD ["8.8.8.8", "-c", "5"]
