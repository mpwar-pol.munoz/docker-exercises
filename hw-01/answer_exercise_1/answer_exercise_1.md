# Diferencia entre el uso de la instrucción CMD y ENTRYPOINT

Cuando definimos imágenes Docker mediante un archivo Dockerfile tenemos dos instrucciones para ejecutar procesos en los contenedores Docker que se crearán: CMD y ENTRYPOINT.

La principal diferencia entre el uso de estas instrucciones es que CMD se puede sobreescribir al ejecutar el contenedor, mientras que si usamos ENTRYPOINT esto no es posible.

## CMD

Así, si creamos una imagen a partir del archivo `example_cmd.Dockerfile` que usa la instrucción CMD:

```
docker build -t test-cmd:1.0.0 -f example_cmd.Dockerfile .
```

![Terminal screenshot](imgs/build_example_cmd.png "Terminal screenshot for the example_cmd build")

Podemos levantar un contenedor con el comando por defecto (ping)

```
docker run --name default_cmd test-cmd:1.0.0
```

![Terminal screenshot](imgs/default_example_cmd.png "Terminal screenshot for the example_cmd default behavior")

O substituirlo por otro:

```
docker run --name overwrite_cmd test-cmd:1.0.0 echo Hello MPWAR
```

![Terminal screenshot](imgs/overwrite_example_cmd.png "Terminal screenshot for a example_cmd overwritten behavior")

## ENTRYPOINT

En cambio, si hacemos lo mismo mediante el archivo `example_entrypoint.Dockerfile` que usa la instrucción ENTRYPOINT:

```
docker build -t test-entrypoint:1.0.0 -f example_entrypoint.Dockerfile .
```

![Terminal screenshot](imgs/build_example_entrypoint.png "Terminal screenshot for the example_entrypoint build")

Podemos levantar un contenedor con el comando por defecto (ping)

```
docker run --name default_entrypoint test-entrypoint:1.0.0
```

![Terminal screenshot](imgs/default_example_entrypoint.png "Terminal screenshot for the example_entrypoint default behavior")

Pero NO substituirlo por otro, ya que lo que añadamos se interpreta como opciones del comando definido en el ENTRYPOINT:

```
docker run --name overwrite_entrypoint test-entrypoint:1.0.0 echo Hello MPWAR
```

![Terminal screenshot](imgs/overwrite_example_entrypoint.png "Terminal screenshot for a example_entrypoint overwritten behavior")

## Combinación

Si bien puede parecer útil tener la flexibilidad de ejecutar cualquier comando al levantar un contenedor, esto conlleva ciertos riesgos de seguridad, incluyendo la posibilidad de escalar con privilegios en la máquina host. 

Por ello, es preferible usar ENTRYPOINT. Si se desea una cierta flexibilidad se puede combinar su uso con el de CMD. 

Así, se puede definir el nombre de la instrucción a ejecutar como ENTRYPOINT y sus parámetros mediante CMD, dejando que el usuario los sobreescriba pero manteniendo los contenedores seguros.

Un ejemplo se puede observar a partir del archivo `example_combo.Dockerfile`.

```
docker build -t test-combo:1.0.0 -f example_combo.Dockerfile .
```

![Terminal screenshot](imgs/build_example_combo.png "Terminal screenshot for the example_combobuild")

Podemos levantar un contenedor con el comando y parámetros por defecto (ping a Google 5 veces)

```
docker run --name default_combo test-combo:1.0.0
```

![Terminal screenshot](imgs/default_example_combo.png "Terminal screenshot for the example_combo default behavior")

Y si añadimos más parámetros substituiremos los definidos en la instrucción CMD:

```
docker run --name overwrite_combo test-combo:1.0.0 8.8.4.4 -c 3
```

![Terminal screenshot](imgs/overwrite_example_combo.png "Terminal screenshot for a example_combo overwritten behavior")
