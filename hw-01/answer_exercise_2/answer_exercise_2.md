# Diferencia entre el uso de la instrucción ADD y COPY

Cuando definimos imágenes Docker mediante un archivo Dockerfile tenemos dos instrucciones para copiar archivos locales a ellas: ADD y COPY.

Si bien ambas se pueden utilizar de la misma forma:

```docker
COPY /origen/documento.txt /destino/
```
```docker
ADD /origen/documento.txt /destino/
```

La instrucción ADD tiene algunas funcionalidades adicionales que no son immediatamente aparentes:

* La dirección de origen del archivo puede ser un enlace URL, haciendo que sus contenidos se descarguen en la imagen.

```docker
ADD https://www.url-origen.com/ejemplo.txt /destino/
```

* Si la dirección de origen del archivo es local y este se identifica como comprimido, se descomprime automáticamente en la imagen.

```docker
ADD /origen/ejemplo.tar.gz /destino/
```

Las [buenas prácticas de docker](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#add-or-copy) recomiendan evitar ADD en la gran mayoría de casos, subsituyendo las funcionalidades adicionales que ofrece por alternativas como la descarga de recursos mediante `curl` o la extracción manual de archivos.