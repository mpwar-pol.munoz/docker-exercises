# Elastic stack Proof of Concept mediante docker-compose

El archivo `docker-compose.yml` está completado según las instrucciones del enunciado. Se puede levantar el sistema ejecutando:

```
docker-compose up
```

Un pequeño detalle a comentar es que se ha explicitado el tipo de driver de la red configurada (bridge) a pesar de ser el por defecto.

![Terminal screenshot](imgs/cmd.png "Terminal screenshot for the exercise")

![Browser screenshot](imgs/browser.png "Browser screenshot for the exercise")
