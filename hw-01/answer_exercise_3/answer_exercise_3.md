# Persistir contenido estático mediante volúmenes

Si bien este ejercicio se podría solucionar mediante bind volumes, enlazando el archivo del host con el contenedor:

```
docker run -it -d -p 8080:80 --name enginequis -v ${PWD}/www:/usr/share/nginx/html nginx:1.19.3-alpine
```

Esta opción no permite poner un nombre al volúmen, como se requiere.

Así, se opta por crear un volúmen vacío con el nombre correspondiente:

```
docker volume create static_content
```

A continuación, se crea un contenedor que lo use:

```
docker run -it -d -p 8080:80 --name enginequis0 -v static_content:/usr/share/nginx/html nginx:1.19.3-alpine
```

Y se copia el contenido que se desea mostrar al contenedor, y por ende al volúmen:

```
docker cp ./www/index.html enginequis0:/usr/share/nginx/html/index.html
```

A partir de este punto cualquier otro contenedor que se ejecute con el mismo volúmen tendrá el mismo archivo:

```
docker run -it -d -p 8081:80 --name enginequis1 -v static_content:/usr/share/nginx/html nginx:1.19.3-alpine
```

![Terminal screenshot](imgs/cmd.png "Terminal screenshot for the exercise")

![Browser screenshot](imgs/browser.png "Browser screenshot for the exercise")

De forma alternativa al paso intermedio de copiar un archivo al contenedor, se podría haber usado la instrucción COPY en un Dockerfile para añadir el archivo a la imagen:

```docker
COPY ./www/index.html /usr/share/nginx/html/index.html
```

La principal desventaja de esta opción es que el archivo aumenta el tamaño de la imagen. 

Además, el uso de volúmenes empezaría a perder sentido, ya que el archivo existiría por defecto en cada contenedor que se levantase a partir de dicha imagen.