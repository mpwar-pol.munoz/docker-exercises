FROM nginx:1.19.3-alpine
EXPOSE 8080
HEALTHCHECK --start-period=15s --interval=45s --timeout=5s --retries=2 \
  CMD curl -f http://localhost:8080 || exit 1
COPY ./www/index.html /usr/share/nginx/html/index.html
COPY ./www/nginx.conf /etc/nginx/nginx.conf
