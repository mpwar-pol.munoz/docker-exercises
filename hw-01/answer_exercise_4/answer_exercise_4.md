# Healthcheck

Puede haber casos en que un contenedor siga ejecutándose pero su proceso haya dejado de funcionar correctamente. Para detectar dichas situaciones podemos usar la instrucción HEALTHCHECK en un Dockerfile.

El archivo `healthcheck.Dockerfile` define una imagen que sirve contenido web en el puerto 8080 de un contenedor, comprovando con los parámetros especificados en el enunciado que el servidor funciona correctamente.

```docker
HEALTHCHECK --start-period=15s --interval=45s --timeout=5s --retries=2 \
  CMD curl -f http://localhost:8080 || exit 1
```

Para realizar dicha comprovación se hace uso del comando `curl` para obtener la página servida. Si todo va bien se devuelve el código 0 (healthy) y en caso contrario 1 (unhealthy).

```
docker build -t healthcheck:1.0.0 -f healthcheck.Dockerfile .
```

```
docker run --name healthcheck -d -p 8080:8080 healthcheck:1.0.0
```

![Terminal screenshot](imgs/cmd.png "Terminal screenshot for the exercise")
