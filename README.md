# docker-exercises

Repository for miscellaneous Docker exercises. MPWAR - La Salle URL. 2020-2021 Academic Year.

**Student:** Pol Muñoz Pastor (pol.munoz)

## hw-01

This folder includes solutions for the first deliverable.